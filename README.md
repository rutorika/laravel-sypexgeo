
Пакет предоставляет команду:

- **rutorika:sypexgeo** - установка или обновление базы SypexGeo

Requirements:

- **php >=7.0.0**
- **laravel >= 5.5**
- **php-zip**

## Postgresql

Команда COPY доступна только для суперпользователя. В настоящий момент обновление базы доступно только через нее.

```
ALTER ROLE <rolename> WITH SUPERUSER;
```

##Gitignore

```
mkdir storage/sypexgeo
echo '*' > storage/sypexgeo/.gitignore && echo '!.gitignore' >> storage/sypexgeo/.gitignore
git add storage/sypexgeo/.gitignore && git commit -m "Sypexgeo stotage gitignore"
```

## Installation

Добавить в **composer.json** репозиторий пакета:

```
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:rutorika/laravel-sypexgeo.git"
    }
]
```

Выполнить:

```
composer require rutorika/laravel-sypexgeo:dev-master

php artisan vendor:publish --provider="Rutorika\Sypexgeo\SypexgeoServiceProvider"
php artisan config:clear
php artisan config:cache
php artisan migrate
php artisan rutorika:sypexgeo
```

## Development installation

Добавить в **composer.json** локальную директорию пакета:

```
"repositories": [
    {
        "type": "path",
        "url": "/srv/www/laravel-sypexgeo"
    }
]
```

Выполнить:

```
composer require rutorika/laravel-sypexgeo:dev-master --prefer-source
composer dump-autoload

php artisan vendor:publish --provider="Rutorika\Sypexgeo\SypexgeoServiceProvider"
php artisan config:clear
php artisan config:cache
php artisan migrate
php artisan rutorika:sypexgeo
```

## Example

```
<?php

    $sypex = new \Rutorika\Sypexgeo\Sypexgeo();
    $ip = $sypex->clientIp();

    $city = $sypex->geoip($ip)->toArray();

    dd(city);

```





