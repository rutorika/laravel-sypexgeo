<?php

namespace Rutorika\Sypexgeo;

use \Rutorika\Sypexgeo\Models\Cities;

class SypexgeoSession
{
    public static function getCity()
    {
        $id = session()->get('sypexgeo.city_id');

        if (empty($cityId)) {
            $sypex = new Sypexgeo();
            $ip    = $sypex->clientIp();
            $city  = $sypex->findCityByIp($ip);
            $id    = $city->id;
            session()->put('sypexgeo.city_id', $id);
        }

        return Cities::where('geo_cities.id', $id)->first();
    }

    public static function setCity(Cities $city)
    {
        session()->put('sypexgeo.city_id', $city->id);
    }

    public static function isCurrent($id)
    {
        $sid = session()->get('sypexgeo.city_id');

        return ($id == $sid);
    }
}