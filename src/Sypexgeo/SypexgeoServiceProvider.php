<?php

namespace Rutorika\Sypexgeo;

class SypexgeoServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config.php' => config_path('rutorika/sypexgeo.php'),
        ]);

        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        if ($this->app->runningInConsole()) {
            $this->commands(
                Commands\SypexgeoCommand::class
            );
        }
    }

    public function provides()
    {
        return [];
    }
}