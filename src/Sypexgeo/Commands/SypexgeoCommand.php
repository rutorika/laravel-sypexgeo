<?php

namespace Rutorika\Sypexgeo\Commands;

use Illuminate\Console\Command;

class SypexgeoCommand extends Command
{
    protected $name = 'rutorika:sypexgeo';

    protected $description = 'Обновление или установка базы SypexGeo';

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $dbUrl   = config('rutorika.sypexgeo.database_url');
        $dictUrl = config('rutorika.sypexgeo.dictionaries_url');

        $this->download($dbUrl);
        $this->download($dictUrl);

        $dbType = $this->dbType();

        $this->updateTable($dbType, 'country.tsv', 'geo_countries');

        $this->updateTable($dbType, 'region.tsv', 'geo_regions', [
            'id',
            'iso',
            'country',
            'name_ru',
            'name_en',
            'timezone',
            'okato' 
        ]);

        $this->updateTable($dbType, 'city.tsv', 'geo_cities', [
            'id',
            'region_id',
            'name_ru',
            'name_en',
            'lat',
            'lon',
            'okato'
        ]);

        $this->line("");
    }

    protected function updateTable($dbType, $file, $table, $fields = null)
    {
        $file = $this->getStoragePath() .'/' . $file;

        if (!file_exists($file)) {
            $this->error('Отсутствует файл ' . $file);
            return;
        }

        $this->line("");
        $this->info("Обновление таблицы $table");

        \DB::table($table)->delete();

        if ($fields !== null && is_array($fields)) {
            $fields = "(" . implode(", ", $fields) . ")";
        }

        if ($dbType == 'pgsql') {
            $sql = "COPY $table $fields FROM '$file'";
        }

        if ($dbType == 'mysql') {
            $sql = "LOAD DATA INFILE '$file' INTO TABLE $table $fields";
        }

        $pdo = \DB::connection()->getPdo();
        $res = $pdo->exec($sql);

        $this->info("Добавлено $res строк");
    }

    protected function dbType()
    {
        $value = config('rutorika.sypexgeo.db_type');
        $value = strtolower($value);

        if (empty($value)) {
            $this->error("Не задан параметр rutorika.sypexgeo.db_type");
            exit;
        }

        if (!in_array($value, ['pgsql', 'mysql'])) {
            $this->error("Неизвестное значение параметр rutorika.sypexgeo.db_type $value");
            exit;
        }

        return $value;
    }

    protected function download($url)
    {
        $this->line("");
        $this->info("Загрузка $url");

        $storage = $this->getStoragePath();

        $tmp = $storage . '/tmp.zip';

        if (file_exists($tmp)) {
            unlink($tmp);
        }

        if (!copy($url, $tmp)) {
            $this->error("Ошибка загрузки файла {$url}");
            $this->line("");
            return;
        }

        $zip = new \ZipArchive();
        if ($zip->open($tmp) !== true) {
            $this->error("Ошибка открытия файла {$tmp}");
            $this->line("");
            return;
        }

        if ($zip->extractTo($storage) === false) {
            $this->error("Ошибка распаковки архива {$tmp}");
            $this->line("");
            return;
        }

        unlink($tmp);

        $this->info("Загрузка выполнена успешно");
    }

    protected function getStoragePath()
    {
        $storage = config()->get('rutorika.sypexgeo.storage');

        if (empty($storage)) {
            $this->error("В конфигурационном файле sypexgeo не задан параметр storage");
            $this->line("");
            exit(1);
        }

        if (!file_exists($storage)) {
            mkdir($storage);
            $gitignore = "*\n!.gitignore\n";
            file_put_contents($storage . '/.gitignore', $gitignore);
        }

        return $storage;
    }
}
