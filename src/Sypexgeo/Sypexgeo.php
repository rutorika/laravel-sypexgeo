<?php

namespace Rutorika\Sypexgeo;

use Rutorika\Sypexgeo\Models\Cities;

class Sypexgeo
{
    protected $config = null;

    protected $sxgeo = null;

    public function getConfig()
    {
        if ($this->config === null) {
            $def = require_once __DIR__ . '/../config.php';
            $app = config()->get('rutorika.sypexgeo');
            $this->config = $app + $def;
        }

        return $this->config;
    }

    /**
     * Библиотека работы с dat
     * @todo удалить ее
     * @return type
     */
    protected function getSxgeo()
    {
        if ($this->sxgeo === null) {

            $cfg = $this->getConfig();
            $dat = $cfg['dat_file'];

            if (!file_exists($dat)) {
                \Log::warning("Отсутствует файл $dat. Выполните команду php artisan rutorika:sypexgeo");
                return null;
            }

            $this->sxgeo = new SxGeo($dat);
        }

        return $this->sxgeo;
    }

    /**
     *
     * @param type $ip
     * @return Cities
     */
    public function findCityByIp($ip)
    {
        $geo = $this->getSxgeo();

        if ($geo !== null) {
            $res = $geo->getCityFull($ip);
            if (!empty($res['city']['id'])) {
                return $this->findCityById($res['city']['id']);
            }
        }

        return null;
    }

    protected function findCityById($id)
    {
        $city = Cities::where('geo_cities.id', $id)->first();

        if (!empty($city->id)) {
            return $city;
        }

        \Log::warning("В таблице geo_cities не найден город #{$id}");
        return null;
    }

    /**
     * Рассчет дистанции между точками в километрах
     *
     * @param type $lat1
     * @param type $lng1
     * @param type $lat2
     * @param type $lng2
     * @return type
     */
    public function distance($lat1, $lng1, $lat2, $lng2)
    {
        $earthRadius = 6371;

        $dLat = deg2rad($lat2 - $lat1);
        $dLng = deg2rad($lng2 - $lng1);

        $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * sin($dLng / 2) * sin($dLng / 2);
        $c = 2 * asin(sqrt($a));
        $d = $earthRadius * $c;

        return $d;
    }

    /**
     * Поиск в базе данных ближайшей географической точки
     *
     * @param type $params[lng]
     * @param type $params[lat]
     * @param type $params[where] whereRaw
     * @return \App\Models\Cities;
     */
    public function findNearestCity($params)
    {
        $cities = Cities::limit(1);

        if (!empty($params['where'])) {

            if (!is_array($params['where'])) {
                $params['where'] = array($params['where']);
            }

            foreach ($params['where'] as $row) {
                $cities->whereRaw($row);
            }
        }

        $cities->orderByRaw(
            "(
                SQRT(
                    POWER(('{$params['lng']}' - lng), 2) + POWER(('{$params['lat']}' - lat), 2)
                )
            )"
        );

        return $cities->first();
    }

    public function defaultCity()
    {
        $cfg = $this->getConfig()['default_city'];

        $model = new Cities();
        $model->fill($cfg);
        $model->country_name_ru = $cfg['country_name_ru'];
        $model->country_name_en = $cfg['country_name_en'];
        $model->region_name_ru  = $cfg['region_name_ru'];
        $model->region_name_en  = $cfg['region_name_en'];

        return $model;
    }
    
    /**
     * Получение ip клиента
     *
     * @return string
     */
    public function clientIp()
    {
        if (getenv('HTTP_CLIENT_IP')) {
            $ip = getenv('HTTP_CLIENT_IP');

        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');

        } else if (getenv('HTTP_X_FORWARDED')) {
            $ip = getenv('HTTP_X_FORWARDED');

        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ip = getenv('HTTP_FORWARDED_FOR');

        } else if (getenv('HTTP_FORWARDED')) {
            $ip = getenv('HTTP_FORWARDED');

        } else if (getenv('REMOTE_ADDR')) {
            $ip = getenv('REMOTE_ADDR');

        } else {
            $ip = 'UNKNOWN';
        }

        return $ip;
    }
}