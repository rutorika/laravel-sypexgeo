<?php

namespace Rutorika\Sypexgeo\Models;

/**
 * Cities
 *
 * @property int     $id
 * @property int     $region_id
 * @property text    $name_ru
 * @property text    $name_en
 * @property numeric $lat
 * @property numeric $lon
 * @property text    $okato
 *
 * @property text    $region_name_ru
 * @property text    $region_name_en
 * @property text    $country_name_ru
 * @property text    $country_name_en
 * 
 * @property text    $location_ru
 * @property text    $location_en
 * 
 * @property Regions $region
 * 
 * @method   static  fetchOrderedRu()
 * @method   static  fetchOrderedEn()
 *
 * @author dm <dmitrij.demidovich@gmail.com>
 */
class Cities extends \Eloquent
{
    protected $table = 'geo_cities';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'region_id',
        'name_ru',
        'name_en',
        'lat',
        'lon',
        'okato',
    ];

    public $timestamps = false;

    public $incrementing = false;

//    protected $appends = ['location_ru', 'location_en'];

//    public function newQuery()
//    {
//        $query = parent::newQuery();
//
//        $query->selectRaw('
//            geo_cities.*,
//            geo_regions.name_ru   AS region_name_ru,
//            geo_regions.name_en   AS region_name_en,
//            geo_countries.name_ru AS country_name_ru,
//            geo_countries.name_en AS country_name_en
//        ');
//
//        $query->join('geo_regions',   'geo_regions.id', '=', 'geo_cities.region_id', 'left');
//        $query->join('geo_countries', 'geo_countries.iso', '=', 'geo_regions.country', 'left');
//
//        return $query;
//    }
//
//    public function getLocationRuAttribute()
//    {
//        $r = [
//            $this->attributes['name_ru'],
//            $this->attributes['country_name_ru']
//        ];
//
//        if (!empty($this->attributes['region_name_ru'])) {
//            $r[] = $this->attributes['region_name_ru'];
//        }
//
//        return implode(", ", $r);
//    }
//
//    public function getLocationEnAttribute()
//    {
//        $r[] = $this->attributes['name_en'];
//
//        if (!empty($this->attributes['region_name_en'])) {
//            $r[] = $this->attributes['region_name_en'];
//        }
//
//        $r[] = $this->attributes['country_name_en'];
//
//        return implode(", ", $r);
//    }

    public static function scopeFetchOrderedRu($query)
    {
        return $query->orderBy('geo_cities.name_ru', 'asc');
    }

    public static function scopeFetchOrderedEn($query)
    {
        return $query->orderBy('geo_cities.name_en', 'asc');
    }

    public function region()
    {
        return $this->belongsTo(Regions::class, 'region_id');
    }
}
