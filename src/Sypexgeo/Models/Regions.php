<?php

namespace Rutorika\Sypexgeo\Models;

use Rutorika\Sypexgeo\Models\Cities;

/**
 * Regions
 *
 * @property int     $id
 * @property text    $iso
 * @property text    $country
 * @property text    $name_ru
 * @property text    $name_en
 * @property text    $timezone
 * @property text    $okato
 * @property Cities[] $cities
 *
 * @author dm <dmitrij.demidovich@gmail.com>
 */
class Regions extends \Eloquent
{
    protected $table = 'geo_regions';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'iso',
        'country',
        'name_ru',
        'name_en',
        'timezone',
        'okato',
    ];

    public $incrementing = false;

    public $timestamps = false;

    public function cities()
    {
        return $this->hasMany(Cities::class, 'region_id');
    }
}
