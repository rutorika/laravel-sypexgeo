<?php

namespace Rutorika\Sypexgeo\Models;

/**
 * Countries
 *
 * @property int     $id
 * @property text    $iso
 * @property text    $country
 * @property text    $name_ru
 * @property text    $name_en
 * @property numeric $lat
 * @property numeric $lon
 * @property text    $timezone
 *
 * @author dm <dmitrij.demidovich@gmail.com>
 */
class Countries extends \Eloquent
{
    protected $table = 'geo_countries';

    protected $fillable = array(
        'iso',
        'continent',
        'name_ru',
        'name_en',
        'lat',
        'lon',
        'timezone',
    );

    protected $guarded = array('id');

    public $timestamps = false;
}
