<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoRegionsTable extends Migration
{
    public function up()
    {
        Schema::create('geo_regions', function(Blueprint $t) {

            $t->integer('id');
            $t->char('iso', 7);
            $t->char('country', 2);
            $t->string('name_ru', 128);
            $t->string('name_en', 128);
            $t->string('timezone', 30);
            $t->char('okato', 4);

            $t->unique('id');
            $t->index('id');
            $t->index('country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_regions');
    }

}
