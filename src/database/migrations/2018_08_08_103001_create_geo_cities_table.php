<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoCitiesTable extends Migration
{
    public function up()
    {
        Schema::create('geo_cities', function(Blueprint $t) {

            $t->integer('id');
            $t->integer('region_id');
            $t->string('name_ru', 128);
            $t->string('name_en', 128);
            $t->decimal('lat', 10, 5);
            $t->decimal('lon', 10, 5);
            $t->char('okato', 20);

            $t->unique('id');
            $t->index('id');
            $t->index('region_id');
            $t->index('name_ru');
            $t->index(['lon', 'lat']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_cities');
    }

}
