<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoCountriesTable extends Migration
{
    public function up()
    {
        Schema::create('geo_countries', function(Blueprint $t) {

            $t->integer('id');
            $t->char('iso', 2);
            $t->char('continent', 2);
            $t->string('name_ru', 128);
            $t->string('name_en', 128);
            $t->decimal('lat', 6, 2);
            $t->decimal('lon', 6, 2);
            $t->string('timezone', 30);

            $t->unique('id');
            $t->index('id');
            $t->index('continent');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_countries');
    }

}
