<?php

return [

    // mysql, pgsql

    'db_type' => 'mysql',

    'database_url' => 'https://sypexgeo.net/files/SxGeoCity_utf8.zip',

    'dictionaries_url' => 'https://sypexgeo.net/files/SxGeo_Info.zip',
    
    'storage' => storage_path() . '/sypexgeo',

    'dat_file' => storage_path() . '/sypexgeo/SxGeoCity.dat',

    'default_country' => [

    ],

    'default_city' => [
        'id' => '524901',
        'region_id' => '524894',
        'name_ru' => 'Москва',
        'name_en' => 'Moscow',
        'lat' => '55.752220000000000',
        'lon' => '37.615560000000000',
        'okato' => '45',
        'region_name_ru' => null,
        'region_name_en' => null,
        'country_name_ru' => 'Россия',
        'country_name_en' => 'Russia'
    ]

];

